import unittest

from models.offer import Offer


class OfferTest(unittest.TestCase):
    def test_format_price_both(self):
        o = Offer(price_s=2.20, price_m=4)
        self.assertEqual(o.format_price(), "💰 2,20€ | 4,00€")

        o.price_s = 2.2
        self.assertEqual(o.format_price(), "💰 2,20€ | 4,00€")
        o.price_m = 2.2232325
        self.assertEqual(o.format_price(), "💰 2,20€ | 2,22€")
        o.price_m = 2.229
        self.assertEqual(o.format_price(), "💰 2,20€ | 2,23€")
        o.price_m = 200.2232325
        self.assertEqual(o.format_price(), "💰 2,20€ | 200,22€")

    def test_format_price_same(self):
        o = Offer(price_m=2, price_s=2)
        self.assertEqual(o.format_price(), "💰 2,00€")

    def test_format_prices_none(self):
        o = Offer(price_m=None, price_s=None)
        self.assertEqual(o.format_price(), "")

    def test_format_prices_single(self):
        o = Offer(price_m=None, price_s=2)
        self.assertEqual(o.format_price(), "💰 2,00€ | N/A€")

    def test_str(self):
        o = Offer("title", "description", 1.2)
        self.assertEqual(str(o), "\n🍽️ title\ndescription\n💰 1,20€ | N/A€\n")
        o.price_m = 45
        self.assertEqual(str(o), "\n🍽️ title\ndescription\n💰 1,20€ | 45,00€\n")
