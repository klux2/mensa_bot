import json
import os.path
import unittest

from griebnitzsee_mensa.ck1parser import CK1Parser


class CK1ParserTest(unittest.TestCase):
    def test_parser(self):
        file = (
            "tests/kitchenone.json"
            if os.path.isfile("tests/kitchenone.json")
            else "kitchenone.json"
        )
        with open(file, "r", encoding="utf-8") as offer_example_file:
            kitchenone = json.loads(offer_example_file.read())

        u = CK1Parser()
        result = u._parse_json(kitchenone)
        self.assertEqual(len(result), 4)

        result = [str(o) for o in result]
        self.assertEqual(
            result,
            [
                "\n🍽️ Campus Kitchen One's Angebot 1 🐖\nKrustenbraten, Wirsing-Kartoffelragout\n\n",
                "\n🍽️ Campus Kitchen One's Angebot 2 🥦\nBalkan Reis, Sojastreifen, Tomatensauce\n\n",
                "\n🍽️ Campus Kitchen One's Angebot 3\nTagesgericht\n\n",
                "\n🍽️ Campus Kitchen One's Angebot 4 🐖\nZwiebel-Maultaschensuppe\n\n",
            ],
        )

    def test_download(self):
        self.assertIsNotNone(CK1Parser()._download_json("2022-11-01"))
