import sys
from datetime import datetime

from griebnitzsee_mensa.manager import Manager


def parse_requested_refectories():
    ulf = "send_ulf" in str(sys.argv).lower()
    mensa = "send_mensa" in str(sys.argv).lower()
    ck1 = "send_ck1" in str(sys.argv).lower()

    if not ulf and not mensa and not ck1:
        ulf = True
        mensa = True
        ck1 = True

    return ulf, mensa, ck1


if __name__ == "__main__test":
    print("ulf is {}, mensa is {}, ck1 is {}".format(*parse_requested_refectories()))

if __name__ == "__main__":
    print(f"Time: {datetime.now()}")

    send_ulf, send_mensa, send_ck1 = parse_requested_refectories()
    manager = Manager(send_ulf, send_mensa, send_ck1)
    error_state = manager.execute()

    if error_state is not None:
        print(error_state.name)
