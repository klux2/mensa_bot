import enum
import os
import traceback
from datetime import datetime
from typing import Tuple, List, Optional

import requests

from griebnitzsee_mensa.ck1parser import CK1Parser
from griebnitzsee_mensa.helpers import save_sent_message, get_last_sent_message
from griebnitzsee_mensa.mensaparser import MensaParser
from griebnitzsee_mensa.ulfparser import UlfParser
from models.offer import Offer

WORK_DAYS = {
    "1": "Monday",
    "2": "Tuesday",
    "3": "Wednesday",
    "4": "Thursday",
    "5": "Friday",
    "6": "Saturday",
}

GERMAN_DAYS = {
    0: "Montag",
    1: "Dienstag",
    2: "Mittwoch",
    3: "Donnerstag",
    4: "Freitag",
    5: "Samstag",
    6: "Sonntag",
}


class Error(enum.Enum):
    MESSAGE_SEND_ERROR = 0
    ZERO_OFFERS = 1
    SHOULD_NOT_SEND = 2
    MISSING_TELEGRAM_ENVARS = 3
    CONTENT_RETRIEVAL_EXCEPTION = 4
    SAME_MESSAGE_CONTENT = 5


class Manager:
    _ulf_parser = UlfParser()
    _mensa_parser = MensaParser()
    _ck1_parser = CK1Parser()

    def __init__(
        self, send_ulf: bool = True, send_mensa: bool = True, send_ck1: bool = True
    ):
        self._send_ulf = send_ulf
        self._send_mensa = send_mensa
        self._send_ck1 = send_ck1
        self._bot_api_key = os.getenv("TELEGRAM_BOT_API_KEY", None)
        self._channel_name = os.getenv("TELEGRAM_BOT_CHANNEL_NAME", None)

    def execute(self) -> Optional[Error]:
        if not (self._bot_api_key and self._channel_name):
            return Error.MISSING_TELEGRAM_ENVARS

        if not self._mensa_parser.should_send_data():
            return Error.SHOULD_NOT_SEND

        try:
            ulf, mensa, ck1 = [], [], []

            if self._send_ulf:
                ulf = self._ulf_parser.get_todays_offers()
            if self._send_mensa:
                mensa = self._mensa_parser.get_todays_offers()
            if self._send_ck1:
                ck1 = self._ck1_parser.get_todays_offers()

            offers, refectory_name = self._get_message_content(ulf, mensa, ck1)
        except:
            traceback.print_exc()
            return Error.CONTENT_RETRIEVAL_EXCEPTION

        if len(offers) == 0:
            return Error.ZERO_OFFERS

        message = self._format_message(refectory_name, offers)

        last_sent_message = get_last_sent_message(refectory_name)
        if last_sent_message == message:
            return Error.SAME_MESSAGE_CONTENT
        else:
            sent_ok = self._send_telegram_message(message)
            save_sent_message(refectory_name, message)

            if sent_ok:
                return None

        return Error.MESSAGE_SEND_ERROR

    def _get_message_content(
        self, ulf: List[Offer], mensa: List[Offer], ck1: List[Offer]
    ) -> Tuple[List[Offer], str]:
        if self._send_ulf and self._send_mensa:
            return [*mensa, *ulf, *ck1], ""
        elif self._send_ulf:
            return ulf, "(Ulf) "
        elif self._send_mensa:
            return mensa, "(Mensa) "
        elif self._send_ck1:
            return ck1, "(Campus Kitchen One) "
        else:
            return [], ""

    @staticmethod
    def _format_message(refectory_name: str, offers: List[Offer]):
        now = datetime.now()
        day_name = GERMAN_DAYS[now.weekday()]

        message = "🍴 Speiseplan {}für {}, den {}\n".format(
            refectory_name, day_name, now.strftime("%d.%m.%Y")
        )

        for offer in offers:
            message += offer.__str__()

        return message

    def _send_telegram_message(self, message: str) -> bool:
        telegram_url = (
            "https://api.telegram.org/bot{}"
            "/sendMessage?chat_id={}&text={}".format(
                self._bot_api_key, self._channel_name, message
            )
        )
        result = requests.get(telegram_url)
        return result.status_code == 200
