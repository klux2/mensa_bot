from griebnitzsee_mensa.openmensaparser import OpenmensaParser

CANTEEN_ID = 1717
MEAL_PREFIX = "Campus Kitchen One's"


class CK1Parser(OpenmensaParser):
    def __init__(self):
        super().__init__(CANTEEN_ID, MEAL_PREFIX)
