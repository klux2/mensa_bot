import json
import urllib.request
from collections import defaultdict
from typing import List, Dict, Optional

from models.offer import Offer

WORK_DAYS = {
    "1": "Monday",
    "2": "Tuesday",
    "3": "Wednesday",
    "4": "Thursday",
    "5": "Friday",
    "6": "Saturday",
}

GERMAN_DAYS = {
    0: "Montag",
    1: "Dienstag",
    2: "Mittwoch",
    3: "Donnerstag",
    4: "Freitag",
    5: "Samstag",
    7: "Sonntag",
}


class MensaParser:
    def __init__(self):
        self._data: Optional[Dict] = None

    def should_send_data(self):
        data = self._get_data()
        return self._is_workday(data) and not self._is_holiday(data)

    def get_todays_offers(self) -> List[Offer]:
        return self._parse_offers(self._get_data()["angebote"])

    def _get_data(self) -> Dict:
        tries = 0
        while not self._data and tries < 3:
            tries += 1
            raw_data = self._download_json()
            try:
                self._data = raw_data["wochentage"][0]["datum"]
            except Exception as e:
                print("The following JSON")
                print(json.dumps(raw_data, indent=4, sort_keys=True))
                print("caused the following exception")
                print(e)

        return self._data

    @staticmethod
    def _download_json() -> Dict:
        uri = (
            "https://www.studentenwerk-potsdam.de/essen/unsere-mensen-cafeterien"
            "/detailinfos/?tx_typoscriptrendering%5Bcontext%5D=%7B%22record%22%3A%22pages"
            "_66%22%2C%22path%22%3A%22tt_content.list.20.ddfmensa_ddfmensajson%22%7D&tx_ddfmensa"
            "_ddfmensajson%5Bmensa%5D=6&cHash=0c7f1095dcc78ff74b6cd32cd231c75f"
        )

        with urllib.request.urlopen(uri) as url:
            data = json.loads(url.read().decode())
            data["status_code"] = url.status
            return data

    @staticmethod
    def _is_workday(data):
        day = str(data["wochentag"])
        return day in WORK_DAYS.keys() or day in WORK_DAYS.values()

    @staticmethod
    def _is_holiday(data):
        if "angebote" not in data:
            return True
        return False

    def _parse_offers(self, raw_offers: List[Dict]) -> List[Offer]:
        def safe_price_conversion(p: Optional[str]) -> Optional[float]:
            if p:
                return float(p)
            return None

        try:
            return list(
                map(
                    lambda ro: Offer(
                        ro.get("titel"),
                        ro.get("beschreibung"),
                        safe_price_conversion(ro.get("preis_s")),
                        safe_price_conversion(ro.get("preis_m")),
                        ro.get("filter", {}).get("zutaten", defaultdict(bool)),
                    ),
                    raw_offers,
                )
            )
        except Exception as e:
            print(json.dumps(self._data, indent=4, sort_keys=True))
            raise e
