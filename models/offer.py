from collections import defaultdict
from typing import Optional

from models.ingredients import Ingredients


class Offer:
    def __init__(
        self,
        title="",
        description="",
        price_s: Optional[float] = -1,
        price_m: Optional[float] = -1,
        ingredients=defaultdict(bool),
    ):
        self.title = self._string_clean(title)
        self.description = self._string_clean(description)
        self.price_s = price_s
        self.price_m = price_m
        self.ingredients = Ingredients(ingredients, "{} {}".format(title, description))

    @staticmethod
    def _string_clean(s: str):
        return (
            s.replace("\n", " ")
            .replace("  ", " ")
            .replace("   ", " ")
            .replace("- ", "-")
            .replace("- ", "-")
            .replace(" - ", "-")
            .replace(" ,", ",")
        )

    def format_price(self):
        def format_german_euros(p: Optional[float]) -> Optional[str]:
            if p and p != -1:
                return "{:.2f}".format(p).replace(".", ",")
            return "N/A"

        if not self.price_s and not self.price_m:
            return ""
        elif self.price_s == self.price_m:
            return "💰 {}€".format(format_german_euros(self.price_s))
        else:
            return "💰 {}€ | {}€".format(
                format_german_euros(self.price_s), format_german_euros(self.price_m)
            )

    def __str__(self):
        return (
            "\n🍽️ {title}{emojies}\n"
            "{description}\n"
            "{price}\n".format(
                title=self.title,
                description=self.description,
                price=self.format_price(),
                emojies=self.ingredients,
            )
        )
